import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Plants from './src/index';
import { store } from "./src/redux/store";
import { Provider } from "react-redux";
// Import the functions you need from the SDKs you need
import { initializeApp, getApps } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDRK3k59wpPHXjz0cspRbXsRTYWUr3eorI",
  authDomain: "plants-bcd8a.firebaseapp.com",
  projectId: "plants-bcd8a",
  storageBucket: "plants-bcd8a.appspot.com",
  messagingSenderId: "513702739932",
  appId: "1:513702739932:web:a318a8c18d50aeeabb1919"
};

// Initialize Firebase
if (!getApps().length) {
  initializeApp(firebaseConfig);
}
// console.log(getApps());

export default function App() {
  return (
    <Provider store={store}>
      <Plants />
    </Provider>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
