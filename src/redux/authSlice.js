import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    email: "",
    usertoken: "",
    jmlcart: "",
};

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        login: (state, action) => {
            state.email = action.payload.email;
            state.usertoken = action.payload.usertoken;
        },
        logout: (state, action) => {
            state.email = "";
            state.usertoken = "";
            state.jmlcart = "";
        },
        badge: (state, action) => {
            state.jmlcart = action.payload.jmlcart;;
        }
    },
});

export const { login, logout, badge } = authSlice.actions;

export default authSlice.reducer;
