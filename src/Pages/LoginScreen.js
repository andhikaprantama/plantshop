import React, { useState } from 'react'
import { View, Text, StyleSheet, ImageBackground, Image, Button, TextInput, TouchableOpacity, Dimensions, Alert, SafeAreaView } from 'react-native'
import { Ionicons } from '@expo/vector-icons';
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";

export default function LoginScreen({ navigation }) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [hidePass, setHidePass] = useState(true);

    const submit = async () => {
        try {
            const auth = getAuth();
            const userCredential = await signInWithEmailAndPassword(auth, email, password);

            const user = userCredential.user;
        } catch (error) {
            const errorCode = error.code;
            const errorMessage = error.message;
            Alert.alert("Registration Failed", errorMessage);
        }
    };
    return (
        <SafeAreaView style={{flex:1}}>
            <ImageBackground
                source={require('../assets/images/bg.jpg')}
                resizeMode="cover" style={styles.imagebackgroud}
            >
                <View style={styles.brandView}>
                    <Image
                        source={require('../assets/images/plantshop.png')}
                        style={{
                            width: 200,
                            height: 200,
                        }}
                    />
                    <Text style={styles.brandViewText}>Welcome to Plants Shop </Text>
                </View>
                <View style={styles.bottomView}>
                    <ImageBackground
                        source={require('../assets/images/daun.png')}
                        resizeMode="cover" style={styles.imagebackgroud2}
                        imageStyle={{ borderRadius: 60 }}
                    >
                        <View style={styles.inputContainer}>
                            <View style={styles.textInputContainer}>
                                <Ionicons
                                    name={'md-mail-outline'}
                                    size={24}
                                    color="grey"
                                />
                                <TextInput
                                    style={styles.input}
                                    placeholder="Email"
                                    value={email}
                                    onChangeText={(value) => setEmail(value)} />
                            </View>
                        </View>
                        <View style={styles.inputContainer}>
                            <View style={styles.textInputContainer}>
                                <Ionicons
                                    name={'lock-closed-outline'}
                                    size={24}
                                    color="grey"
                                />
                                <TextInput
                                    secureTextEntry={hidePass ? true : false}
                                    style={styles.input}
                                    placeholder="Password"
                                    value={password}
                                    onChangeText={(value) => setPassword(value)} />
                                <Ionicons
                                    name={hidePass ? 'eye-off' : 'eye'}
                                    size={24}
                                    color="grey"
                                    onPress={() => setHidePass(!hidePass)}
                                />
                            </View>

                        </View>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={submit}
                        >
                            <Text style={styles.buttonText}>Sign In</Text>
                        </TouchableOpacity>
                        <View style={styles.atauContainer}>
                            <Text style={styles.atauText}>
                                Didn’t have any account?
                            </Text>
                            <Text style={styles.atauText2} onPress={() => navigation.navigate('RegisterScreen')}> Sign Up here</Text>
                        </View>
                    </ImageBackground>

                </View>
            </ImageBackground>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center'
    },
    imageContainer: {
        marginTop: 100
    },
    imagebackgroud: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
    },
    imagebackgroud2: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        borderTopStartRadius: 60,
        borderTopEndRadius: 60,
    },
    title: {
        marginTop: 25,
        fontSize: 30,
        marginBottom: 25
    },
    inputContainer: {
        marginBottom: 25,
        width: '80%'
    },
    inputLabel: {
        fontSize: 16,
    },
    textInputContainer: {
        borderWidth: 1,
        borderColor: 'white',
        backgroundColor: "white",
        borderRadius: 9,
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 10,
        paddingHorizontal: 5
    },
    input: {
        flex: 1,
        // borderWidth: 1,
        // borderColor: 'black',
        paddingHorizontal: 10
    },
    button: {
        borderRadius: 20,
        borderWidth: 1,
        backgroundColor: '#0ACF83',
        borderColor: '#0ACF83',
        height: '15%',
        width: '80%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        color: 'white',
        fontSize: 20
    },
    atauText: {
        fontSize: 16,
        color: 'white'
    },
    atauText2: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#0ACF83'
    },
    atauContainer: {
        marginVertical: 15,
        flexDirection: "row"
    },
    brandViewText: {
        color: '#FFFFFF',
        fontSize: 20,
        fontWeight: 'bold',
        textTransform: 'uppercase',
    },
    bottomView: {
        flex: 1,
        borderTopStartRadius: 60,
        borderTopEndRadius: 60,
        width: '100%',
    },
    brandView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
})



