import React from 'react'
import { StyleSheet, View, Text, Image, SafeAreaView, TouchableOpacity } from "react-native";
import { Ionicons, FontAwesome5, MaterialCommunityIcons, FontAwesome } from '@expo/vector-icons';
import { getAuth, onAuthStateChanged, signOut } from "firebase/auth";
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../redux/authSlice';


export default function AboutScreen() {
    const dispatch = useDispatch();
    const userName = useSelector((state) => state.auth.email);
    return (
        <SafeAreaView
            style={{ flex: 1, paddingHorizontal: 20, backgroundColor: "white" }}>

            <View style={styles.header}>
                <View>
                    <Text style={styles.textHeader}>Profile</Text>
                    {/* <Text style={{ fontSize: 25, fontWeight: 'bold' }}>Welcome to</Text>
                    <Text style={{ fontSize: 38, color: COLORS.green, fontWeight: 'bold' }}>
                        Plant Shop
                    </Text> */}
                </View>
                <Ionicons name="md-basket-outline" size={24} />
                {/* <Icon name="shopping-cart" size={28} /> */}
            </View>
            <View style={styles.imageContainer}>
                <Image style={styles.image} source={require('../assets/images/user-.png')} />
            </View>
            <View style={styles.nameContainer}>
                <Text style={styles.nameText}>
                    {userName}
                </Text>
            </View>
            <View style={styles.portofolioContainer}>
                <View style={styles.portofolioTextContainer}>
                    <Text style={styles.portofolioText}>
                        Pengaturan Akun
                    </Text>
                </View>
                <View style={styles.portofolioHeader}>
                    <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                        <FontAwesome5 name="address-book" size={24} color="#0ACF83" />
                        <Text style={styles.portfolioItemText}>Daftar Alamat</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                        <MaterialCommunityIcons name="bank" size={24} color="#0ACF83" />
                        <Text style={styles.portfolioItemText}>Rekening Bank</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                        <MaterialCommunityIcons name="security" size={24} color="#0ACF83" />
                        <Text style={styles.portfolioItemText}>Keamanan Akun</Text>
                    </View>
                </View>
            </View>
            <View style={styles.portofolioContainer}>
                <View style={styles.portofolioTextContainer}>
                    <Text style={styles.portofolioText}>
                        Seputar Plant Shop
                    </Text>
                </View>
                <View style={styles.portofolioHeader}>
                    <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                        <Ionicons name="leaf-outline" size={24} color="#0ACF83" />
                        <Text style={styles.portfolioItemText}>Kenali Plantshop</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                        <FontAwesome name="book" size={24} color="#0ACF83" />
                        <Text style={styles.portfolioItemText}>Syarat dan Ketentuan</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                        <MaterialCommunityIcons name="application" size={24} color="#0ACF83" />
                        <Text style={styles.portfolioItemText}>Ulas Aplikasi ini</Text>
                    </View>
                </View>
            </View>
            <TouchableOpacity
                style={styles.button}
                onPress={() => {
                    const auth = getAuth();
                    signOut(auth);
                    dispatch(logout());
                }}
            >
                <Text style={styles.buttonText}>Sign Out</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white'
    },
    header: {
        marginTop: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    textHeader: {
        fontSize: 30,
    },
    titleContainer: {
        marginTop: 100
    },
    title: {
        fontSize: 30,
        color: '#003366'
    },
    imageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 25

    },
    image: {
        borderRadius: 100,
        overflow: 'hidden',
        height: 200,
        width: 200,
    },
    nameContainer: {
        marginTop: 10,
        justifyContent: "center",
        alignItems: "center",
        marginBottom:20
    },
    nameText: {
        color: "black",
        fontSize: 25,
        justifyContent: "center",
        alignItems: "center"
    },
    jobContainer: {
        marginBottom: 10
    },
    jobText: {
        color: "#3EC6FF",
        fontSize: 16
    },
    portofolioContainer: {
        backgroundColor: '#F5F7FA',
        width: '100%',
        marginBottom: 10,
        borderRadius: 20

    },
    portofolioTextContainer: {
        borderBottomWidth: 1,
        borderColor: 'black',
        width: '100%',
        padding: 5,
    },
    portofolioText: {

    },
    portfolioItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        padding: 10
    },
    portfolioItem: {
        alignItems: 'center'
    },
    portfolioItemText: {
        color: 'black',
        fontSize: 20,
        marginLeft: 20
    },
    portofolioHeader: {
        padding: 10,
    },
    button: {
        borderRadius: 25,
        backgroundColor: '#0ACF83',
        paddingVertical: 10,
        paddingHorizontal: 35,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        color: 'white',
        fontSize: 20
    },

});
