import React, { useState, useEffect } from 'react'
import { Button, StyleSheet, Text, View, TextInput, TouchableOpacity, SafeAreaView, FlatList, Image } from 'react-native'
import { collection, getDocs, getFirestore, onSnapshot, doc, addDoc, deleteDoc, query, where } from "firebase/firestore";
import { Feather, Ionicons } from '@expo/vector-icons';
import { useSelector, useDispatch } from 'react-redux';


export default function CartScreen() {
    const [listcart, setListcart] = useState([])
    // console.log(listcart);
    const barbadge = useSelector((state) => state.auth.jmlcart);
    const userName = useSelector((state) => state.auth.email);
    useEffect(() => {
        const db = getFirestore();
        // var datas = [];
        // const q = query(collection(db, "cart"), where("email", "==", userName));

        const unsub = onSnapshot(query(collection(db, "cart"), where("email", "==", userName)), (collection) => {
            const carts = [];
            collection.forEach((doc) => {
                carts.push({ ...doc.data(), id: doc.id });
                // console.log(doc.data());
            });
            // console.log(datas);
            setListcart(carts);
        });

    }, []);
    const deleteItem = async (item) => {

        const db = getFirestore();
        const deletes = await deleteDoc(doc(db, "cart", item));
        // console.log(deletes);
    }
    const currencyFormat = (num) => {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };
    const images = (num) => {
        switch (num) {
            case "crassula-ovata.png":
                return require('../assets/images/crassula-ovata.png')
                break;
            case "nephrolepis.png":
                return require('../assets/images/nephrolepis.png')
                break;
            case "asplenium-nidus.png":
                return require('../assets/images/asplenium-nidus.png')
                break;
            case "calathea.png":
                return require('../assets/images/calathea.png')
                break;
            case "peacelily.png":
                return require('../assets/images/peacelily.png')
                break;
            case "peperomia.png":
                return require('../assets/images/peperomia.png')
                break;
        }
    };
    let totalPrice = 0;
    listcart.forEach((item) => {
        totalPrice += item.harga;

    })
    let subtotalPrice = totalPrice + 100000 + 50000;

    return (

        <SafeAreaView
            style={{ flex: 1, paddingHorizontal: 20, backgroundColor: "white" }}>
            <View style={styles.header}>
                <View>
                    <Text style={styles.textHeader}>Cart</Text>
                    {/* <Text style={{ fontSize: 25, fontWeight: 'bold' }}>Welcome to</Text>
                    <Text style={{ fontSize: 38, color: COLORS.green, fontWeight: 'bold' }}>
                        Plant Shop
                    </Text> */}
                </View>
                <Ionicons name="md-basket-outline" size={24} />
                {/* <Icon name="shopping-cart" size={28} /> */}
            </View>
            <FlatList

                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    marginTop: 10,
                    paddingBottom: 50,
                }}

                data={listcart}
                keyExtractor={(item, index) => `${item.id}-${index}`}
                renderItem={({ item }) => {
                    return (
                        <View style={styles.card}>
                            <View style={{ flexDirection: "row", justifyContent: 'space-between', }}>
                                <View style={{ flexDirection: "column" }}>

                                    <View
                                        style={{
                                            height: 80,
                                            alignItems: 'center',
                                        }}>
                                        <Image
                                            source={images(item.image)}
                                            style={{ flex: 1, resizeMode: 'contain', height: 60, width: 150 }}
                                        />
                                    </View>
                                </View>
                                <View style={{ flexDirection: "column" }}>
                                    <Text style={{ fontSize: 15, marginTop: 5 }}>
                                        {item.type}
                                    </Text>
                                    <Text style={{ fontWeight: 'bold', fontSize: 19, marginTop: 5 }}>
                                        {item.title}
                                    </Text>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            marginTop: 5,
                                        }}>
                                        <Text style={{ fontSize: 19, fontWeight: 'bold' }}>
                                            {currencyFormat(Number(item.harga))}
                                        </Text>




                                    </View>
                                </View>
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    onPress={() => deleteItem(item.id)}
                                >
                                    <View
                                        style={{
                                            height: 25,
                                            width: 25,
                                            backgroundColor: "#0ACF83",
                                            borderRadius: 5,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}>
                                        <Text
                                            style={{ fontSize: 20, color: "white", fontWeight: 'bold' }}>
                                            x
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>



                        </View>
                    )
                }}
            />
            {!barbadge ?
                <View style={styles.card}>
                    <Text>No Item</Text>
                </View>
                :
                <View style={styles.card}>
                    <View style={{ flexDirection: "row", justifyContent: 'space-between' }}>
                        <Text>Item ({barbadge}): </Text>
                        <Text>{currencyFormat(Number(totalPrice))}</Text>
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: 'space-between', marginTop: 5 }}>
                        <Text>Shipping :</Text>
                        <Text>RP 100.000</Text>
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: 'space-between', marginTop: 5 }}>
                        <Text>Import charges :</Text>
                        <Text>RP 50.000</Text>
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: 'space-between', marginTop: 20 }}>
                        <Text>Total Price :</Text>
                        <Text>{currencyFormat(Number(subtotalPrice))}</Text>
                    </View>
                    {/* <Icon name="shopping-cart" size={28} /> */}
                </View>
            }

            <TouchableOpacity
                style={styles.button}
            // onPress={() => addData(details)}
            >
                <Text style={styles.buttonText}>Check Out</Text>
            </TouchableOpacity>
        </SafeAreaView>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    header: {
        marginTop: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    textHeader: {
        fontSize: 30,
    },
    card: {
        height: 120,
        backgroundColor: "#F5F7FA",
        marginHorizontal: 2,
        borderRadius: 10,
        marginBottom: 20,
        padding: 15,
    },
    button: {
        borderRadius: 25,
        backgroundColor: '#0ACF83',
        paddingVertical: 10,
        paddingHorizontal: 35,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        color: 'white',
        fontSize: 20
    },
})
