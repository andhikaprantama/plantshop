import React,{useState, useEffect} from 'react'
import { Button, SafeAreaView, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import { Feather, Ionicons, MaterialIcons } from '@expo/vector-icons';
import { collection, getDocs, getFirestore, onSnapshot, doc, addDoc  } from "firebase/firestore";
import { useSelector, useDispatch } from 'react-redux';

export default function DetailScreen({ navigation, route }) {
    const { details } = route.params;
    const userName = useSelector((state) => state.auth.email);
    const currencyFormat = (num) => {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };
    // console.log(details);
    const images = (num) => {
        switch (num) {
            case "crassula-ovata.png":
                return require('../assets/images/crassula-ovata.png')
                break;
            case "nephrolepis.png":
                return require('../assets/images/nephrolepis.png')
                break;
            case "asplenium-nidus.png":
                return require('../assets/images/asplenium-nidus.png')
                break;
            case "calathea.png":
                return require('../assets/images/calathea.png')
                break;
            case "peacelily.png":
                return require('../assets/images/peacelily.png')
                break;
            case "peperomia.png":
                return require('../assets/images/peperomia.png')
                break;
        }
    };
    const addData = async (item) => {
        const db = getFirestore();
        try {
            const docRef = await addDoc(collection(db, "cart"), {
                idprod: item.id,
                title: item.title,
                type: item.type,
                desc: item.desc,
                harga: item.harga,
                image: item.image,
                email: userName
            });
            console.log("Document written with ID: ", docRef.id);
        } catch (e) {
            console.error("Error adding document: ", e);
        }


    }
    return (
        // <>
        // <Button title='Logout' onPress={() => {
        //         const auth = getAuth();
        //         signOut(auth);
        //         dispatch(logout());
        //     }} />
        // </>

        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: "white",
            }}>
            <View style={styles.header}>
                <MaterialIcons name="arrow-back" size={28} onPress={() => navigation.goBack()} />
                <MaterialIcons name="shopping-cart" size={28} />
            </View>
            <View style={styles.imageContainer}>
                <Image source={images(details.image)} style={{ resizeMode: 'contain', flex: 1 }} />
            </View>
            <View style={styles.detailsContainer}>
                <View
                    style={{
                        marginLeft: 20,
                        flexDirection: 'row',
                        alignItems: 'flex-end',
                    }}>
                    <Text style={{ fontSize: 20,  }}>{details.type}</Text>
                </View>
                <View
                    style={{
                        marginLeft: 20,
                        marginTop: 20,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                    }}>
                    <Text style={{ fontSize: 22, fontWeight: 'bold' }}>{details.title}</Text>
                    <View style={styles.priceTag}>
                        <Text
                            style={{
                                marginLeft: 15,
                                color: "white",
                                fontWeight: 'bold',
                                fontSize: 16,
                            }}>
                            {currencyFormat(Number(details.harga))}
                        </Text>
                    </View>
                </View>
                <View style={{ paddingHorizontal: 20, marginTop: 10 }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>About</Text>
                    <Text
                        style={{
                            color: 'grey',
                            fontSize: 16,
                            lineHeight: 22,
                            marginTop: 10,
                        }}>
                        {details.desc}
                    </Text>
                    
                        
                </View>
                <TouchableOpacity
                            style={styles.button}
                            onPress={() => addData(details)}
                        >
                            <Text style={styles.buttonText}>Add to Cart</Text>
                        </TouchableOpacity>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    header: {
        paddingHorizontal: 20,
        marginTop: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    imageContainer: {
        flex: 0.45,
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    detailsContainer: {
        flex: 0.55,
        // backgroundColor: COLORS.light,
        marginHorizontal: 7,
        marginBottom: 7,
        borderRadius: 20,
        marginTop: 30,
        paddingTop: 30,
    },
    line: {
        width: 25,
        height: 2,
        // backgroundColor: COLORS.dark,
        marginBottom: 5,
        marginRight: 3,
    },
    borderBtn: {
        borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        width: 60,
        height: 40,
    },
    borderBtnText: { fontWeight: 'bold', fontSize: 28 },
    buyBtn: {
        width: 130,
        height: 50,
        backgroundColor: '#0ACF83',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
    },
    priceTag: {
        backgroundColor: '#0ACF83',
        width: 100,
        height: 40,
        justifyContent: 'center',
        borderTopLeftRadius: 25,
        borderBottomLeftRadius: 25,
    },
    button: {
        borderRadius: 56,
        borderWidth: 1,
        backgroundColor: '#0ACF83',
        borderColor: '#0ACF83',
        height: '15%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
    },
    buttonText: {
        color: 'white',
        fontSize: 20
    },
})
