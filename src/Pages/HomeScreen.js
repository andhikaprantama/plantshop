import React, { useState, useEffect } from 'react'
import { Button, StyleSheet, Text, View, TextInput, TouchableOpacity, SafeAreaView, FlatList, Image, Alert } from 'react-native'
import { getAuth, onAuthStateChanged, signOut } from "firebase/auth";
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../redux/authSlice';
import { Feather, Ionicons } from '@expo/vector-icons';
import { collection, getDocs, getFirestore, onSnapshot, doc, addDoc  } from "firebase/firestore";

export default function HomeScreen({ navigation, route }) {
    const dispatch = useDispatch();
    const [total, setTotal] = useState(0);
    const userName = useSelector((state) => state.auth.email);
    const [list, setList] = useState([])
    const currencyFormat = (num) => {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };
    // const images = (img) => {
    //     var imageName = require('../assets/images/'+img+'');
    //     console.log(imageName);
    //     return imageName;
    // }

    const images = (num) => {
        switch (num) {
            case "crassula-ovata.png":
                return require('../assets/images/crassula-ovata.png')
                break;
            case "nephrolepis.png":
                return require('../assets/images/nephrolepis.png')
                break;
            case "asplenium-nidus.png":
                return require('../assets/images/asplenium-nidus.png')
                break;
            case "calathea.png":
                return require('../assets/images/calathea.png')
                break;
            case "peacelily.png":
                return require('../assets/images/peacelily.png')
                break;
            case "peperomia.png":
                return require('../assets/images/peperomia.png')
                break;
        }
    };
    useEffect(() => {
        const db = getFirestore();
        // var datas = [];
        const unsub = onSnapshot(collection(db, "product"), (collection) => {
            const datas = [];
            collection.forEach((doc) => {
                datas.push(doc.data());
                // console.log(doc.data());
            });
            // console.log(datas);
            setList(datas);
        });

    }, []);

    const addData = async (item) => {
        const db = getFirestore();
        try {
            const docRef = await addDoc(collection(db, "cart"), {
                idprod: item.id,
                title: item.title,
                type: item.type,
                desc: item.desc,
                harga: item.harga,
                image: item.image,
                email: userName
            });
            console.log("Document written with ID: ", docRef.id);
            Alert.alert('Success','Sent to basket.');
        } catch (e) {
            console.error("Error adding document: ", e);
        }


    }




    return (
        <SafeAreaView
            style={{ flex: 1, paddingHorizontal: 20, backgroundColor: "white" }}>
            <View style={styles.header}>
                <View>
                    <Text style={styles.textHeader}>Discover</Text>
                    {/* <Text style={{ fontSize: 25, fontWeight: 'bold' }}>Welcome to</Text>
                    <Text style={{ fontSize: 38, color: COLORS.green, fontWeight: 'bold' }}>
                        Plant Shop
                    </Text> */}
                </View>
                <Feather name="bell" size={24} />
                {/* <Icon name="shopping-cart" size={28} /> */}
            </View>
            <View style={{ marginTop: 30, flexDirection: 'row' }}>
                <View style={styles.searchContainer}>
                    <Ionicons
                        name={'search'}
                        size={24}
                        color="grey"
                    />
                    <TextInput placeholder="Search" style={styles.input} />
                </View>
            </View>
            <View style={styles.categoryContainer}>
                <TouchableOpacity
                    style={styles.button}>
                    <Text>Top picks</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{ ...styles.button, ...{ backgroundColor: "#F5F7FA", } }}>
                    <Text>Indoor</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{ ...styles.button, ...{ backgroundColor: "#F5F7FA", } }}>
                    <Text>Garden</Text>
                </TouchableOpacity>
            </View>
            <FlatList
                columnWrapperStyle={{ justifyContent: 'space-between' }}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    marginTop: 10,
                    paddingBottom: 50,
                }}
                numColumns={2}
                data={list}
                keyExtractor={(item, index) => `${item.id}-${index}`}
                renderItem={({ item }) => {
                    return (

                        <View style={styles.card}>
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => { navigation.navigate('DetailScreen', { details: item }) }}
                            >
                                <View style={{ alignItems: 'flex-end' }}>
                                    <View
                                        style={{
                                            width: 30,
                                            height: 30,
                                            borderRadius: 20,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}>
                                        {/* <Icon
                                            name="favorite"
                                            size={18}
                                            color={plant.like ? COLORS.red : COLORS.black}
                                        /> */}
                                    </View>
                                </View>

                                <View
                                    style={{
                                        height: 80,
                                        alignItems: 'center',
                                    }}>
                                    <Image
                                        source={images(item.image)}
                                        style={{ flex: 1, resizeMode: 'contain', height: 60, width: 150 }}
                                    />
                                </View>
                            </TouchableOpacity>

                            <Text style={{ fontSize: 15, marginTop: 10 }}>
                                {item.type}
                            </Text>
                            <Text style={{ fontWeight: 'bold', fontSize: 19, marginTop: 10 }}>
                                {item.title}
                            </Text>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginTop: 5,
                                }}>
                                <Text style={{ fontSize: 19, fontWeight: 'bold' }}>
                                    {currencyFormat(Number(item.harga))}
                                </Text>
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    onPress={() => addData(item)}
                                >
                                    <View
                                        style={{
                                            height: 25,
                                            width: 25,
                                            backgroundColor: "#0ACF83",
                                            borderRadius: 5,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}>
                                        <Text
                                            style={{ fontSize: 22, color: "white", fontWeight: 'bold' }}>
                                            +
                                        </Text>
                                    </View>
                                </TouchableOpacity>

                            </View>
                        </View>

                    )
                }}
            />
        </SafeAreaView>

        // <View style={styles.container}>
        //     <Text>HomeScreen</Text>
        //     <Text>{userName}</Text>
        //     <Button title='Logout' onPress={() => {
        //         const auth = getAuth();
        //         signOut(auth);
        //         dispatch(logout());
        //     }} />
        // </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white"
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    header: {
        marginTop: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    searchContainer: {
        height: 50,
        backgroundColor: '#F5F7FA',
        borderRadius: 10,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    categoryContainer: {
        flexDirection: 'row',
        marginTop: 30,
        marginBottom: 20,
        justifyContent: 'space-between',
    },
    card: {
        height: 225,
        backgroundColor: "#F5F7FA",
        marginHorizontal: 2,
        borderRadius: 10,
        marginBottom: 20,
        padding: 15,
    },
    // header: {
    //     flex: 1,
    //     flexDirection: "row",
    //     alignItems: "flex-start",
    //     justifyContent: "space-between",
    //     padding: 40
    // },
    textHeader: {
        fontSize: 30,
    },
    inputContainer: {
        flex: 2,
        marginBottom: 25,
        width: '80%'
    },
    textInputContainer: {

        backgroundColor: "#F5F7FA",
        borderRadius: 15,
        flexDirection: "row",
        alignItems: "flex-start",
        paddingVertical: 10,
        paddingHorizontal: 5
    },
    input: {
        // flex: 1,
        // borderWidth: 1,
        // borderColor: 'black',
        paddingHorizontal: 10
    },
    typeheader: {
        flexDirection: "row"
    },
    button: {
        borderRadius: 25,
        backgroundColor: '#0ACF83',
        paddingVertical: 10,
        paddingHorizontal: 35,
        justifyContent: 'center',
        alignItems: 'center'
    },
})

