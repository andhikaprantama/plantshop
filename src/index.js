import React, { useState, useEffect } from 'react'
import LoginScreen from './Pages/LoginScreen';
import RegisterScreen from './Pages/RegisterScreen';
import HomeScreen from './Pages/HomeScreen';
import AboutScreen from './Pages/AboutScreen';
import CartScreen from './Pages/AddScreen';
import DetailScreen from './Pages/DetailScreen';
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { useSelector, useDispatch } from 'react-redux';
import { collection, getDocs, getFirestore, onSnapshot, doc, addDoc, query, where } from "firebase/firestore";
import { badge, login } from './redux/authSlice';
import { LogBox } from 'react-native';


const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
LogBox.ignoreLogs(['Remote debugger is in a background'])
LogBox.ignoreLogs(['AsyncStorage has been extracted from'])
export default function index() {

  // const [usertoken, setusertoken] = useState("");
  const userName = useSelector((state) => state.auth.email);
  const usertoken = useSelector((state) => state.auth.usertoken);
  const dispatch = useDispatch();
  // console.log(email);
  useEffect(() => {
    const auth = getAuth();
      onAuthStateChanged(auth, (user) => {
        if (user) {
          // User is signed in, see docs for a list of available properties
          // https://firebase.google.com/docs/reference/js/firebase.User
          const uid = user.uid;
          // console.log(uid);
          dispatch(login({
            email: user.email,
            usertoken: uid
          }));

          // setusertoken(user.getIdToken());
        } else {
          // User is signed out
          // setusertoken("");
        }
      });


  }, []);
      const db = getFirestore();
      // var datas = [];
      const unsub = onSnapshot(query(collection(db, "cart"), where("email", "==", userName)), (collection) => {
        const datas = [];
        collection.forEach((doc) => {
          datas.push({ ...doc.data(), id: doc.id });
          // console.log(doc.data());
        });
        // console.log(datas);
        dispatch(badge({
          jmlcart: datas.length,
        }));
      });


  return (

    <NavigationContainer>
      <Stack.Navigator>
        {!usertoken ? (
          <>
            <Stack.Screen name="LoginScreen" component={LoginScreen} options={{ headerShown: false }} />
            <Stack.Screen name="RegisterScreen" component={RegisterScreen} options={{ headerShown: false }} />
          </>) : (
          <>
            <Stack.Screen name="App" component={MainApp} options={{ headerShown: false }} />
            <Stack.Screen name="DetailScreen" component={DetailScreen} options={{ headerShown: false }} />
          </>

        )}
      </Stack.Navigator>
    </NavigationContainer>

  )
}

const MainApp = () => {
  const barbadge = useSelector((state) => state.auth.jmlcart);
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === 'HomeScreen') {
            iconName = focused
              ? 'md-leaf'
              : 'md-leaf-outline';
          } else if (route.name === 'CartScreen') {
            iconName = focused ? 'md-basket' : 'md-basket-outline';
          } else {
            iconName = focused ? 'person' : 'person-outline';
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: '#0ACF83',
        tabBarInactiveTintColor: 'gray',
        headerShown: false
      })}>
      <Tab.Screen name="HomeScreen" component={HomeScreen} />
      <Tab.Screen name="CartScreen" component={CartScreen} options={{ tabBarBadge: barbadge }} />
      <Tab.Screen name="AboutScreen" component={AboutScreen} />
    </Tab.Navigator>
  )
}